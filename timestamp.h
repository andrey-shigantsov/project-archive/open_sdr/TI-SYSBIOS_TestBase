/*
 * timestamp.h
 *
 *  Created on: 3 окт. 2018 г.
 *      Author: svetozar
 */

#ifndef SYSTEM_TIMESTAMP_H_
#define SYSTEM_TIMESTAMP_H_

#include <c6x.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef long long timestamp_t;
static inline void init_timestamp_system(){TSCL=0,TSCH=0;}
static inline timestamp_t timestamp_clk(){return _itoll(TSCH,TSCL);}

#ifdef __cplusplus
}
#endif

#endif /* SYSTEM_TIMESTAMP_H_ */
