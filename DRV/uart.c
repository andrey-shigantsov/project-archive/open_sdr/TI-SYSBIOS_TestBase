/*
 * uart.c
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#include <sys_log_cfg.h>
#define LOG_NAME "Driver.UART"
#define LOG_LEVEL DRIVER_UART_LOG_LEVEL

#include "../log.h"
#include "uart.h"

#include <xdc/cfg/global.h>

#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>

#include <ti/drv/uart/UART.h>

#include <string.h>

#ifndef SYS_DRV_UART_TX_TIMEOUT
#define SYS_DRV_UART_TX_TIMEOUT BIOS_WAIT_FOREVER
#endif
#ifndef SYS_DRV_UART_RX_TIMEOUT
#define SYS_DRV_UART_RX_TIMEOUT 250
#endif

#define UART_INSTANCE 2
#define UART_BAUDRATE 115200

static inline const char * uart_transfer_status_str(UART_TransferStatus status)
{
  switch (status)
  {
  case UART_TRANSFER_STATUS_SUCCESS:
    return "successful";
  case UART_TRANSFER_STATUS_TIMEOUT:
    return "Time out";
  case UART_TRANSFER_STATUS_ERROR_BI:
    return "Break condition error";
  case UART_TRANSFER_STATUS_ERROR_FE:
    return "Framing error";
  case UART_TRANSFER_STATUS_ERROR_PE:
    return "Parity error";
  case UART_TRANSFER_STATUS_ERROR_OE:
    return "Overrun error";
  case UART_TRANSFER_STATUS_ERROR_OTH:
    return "Other error";
  default:
    return "Unknown error";
  }
}

#define HANDLE_TRANSFER_STATUS(status)\
do{ \
  const char * __status_str = uart_transfer_status_str(status); \
  switch (status) \
  { \
  case UART_TRANSFER_STATUS_SUCCESS: \
  case UART_TRANSFER_STATUS_TIMEOUT: \
    LOG_FXN(TRACE, "%s", __status_str); \
    break; \
  case UART_TRANSFER_STATUS_ERROR_FE: \
    LOG_FXN(ERROR, "%s", __status_str); \
    UART_close(This->handle); \
    break; \
  case UART_TRANSFER_STATUS_ERROR_BI: \
  case UART_TRANSFER_STATUS_ERROR_PE: \
  case UART_TRANSFER_STATUS_ERROR_OE: \
  case UART_TRANSFER_STATUS_ERROR_OTH: \
  default: \
    LOG_FXN(WARNING, "%s", __status_str); \
    UART_close(This->handle); \
    open(This); \
    break; \
  } \
}while(0)

static void open(Drv_UART_t * This);

volatile bool drv_UART_rxTask_isRunning = 0;

void drv_uart_rx_task_fxn(UArg a0, UArg a1)
{
  LOG_FXN_TRACE(START);

  Drv_UART_t * This = (Drv_UART_t *)a0;

  LOG_FXN_TRACE(RUNNING);

  drv_UART_rxTask_isRunning = 1;
  while(drv_UART_rxTask_isRunning)
  {
    UART_Transaction transaction;
    transaction.buf = (void *)This->rxBuf;
    transaction.timeout = SYS_DRV_UART_RX_TIMEOUT;
    transaction.count = (uint32_t)This->rxBufSize;
    int32_t err = UART_read2(This->handle, &transaction);
    HANDLE_TRANSFER_STATUS(transaction.status);
    if (!transaction.status && (err == UART_ERROR))
      LOG_FXN(ERROR,"read return error");
    if (transaction.count <= 0) continue;
    assert(transaction.count <= This->rxBufSize);
    if (This->cbk.on_ready_read)
      This->cbk.on_ready_read(This->Parent, (uint8_t *)This->rxBuf, transaction.count);
  }

  LOG_FXN_TRACE(FINISH);
}

#define TX_LOCK_TIMEOUT 10000
static inline void tx_lock(Drv_UART_t * This)
{
  if (!Semaphore_pend(This->txMutex,TX_LOCK_TIMEOUT))
  {
    LOG(WARNING,"TX: lock timeout exited, wait forever...");
    Semaphore_pend(This->txMutex, BIOS_WAIT_FOREVER);
    LOG(WARNING,"TX: locked");
  }
}
static inline void tx_unlock(Drv_UART_t * This){Semaphore_post(This->txMutex);}
int32_t drv_uart_write(Drv_UART_t * This, const char * buf, size_t size)
{
  LOG_FXN_TRACE(START);
  UART_Transaction transaction;
  transaction.buf = (void *)buf;
  transaction.timeout = SYS_DRV_UART_TX_TIMEOUT;
  transaction.count = (uint32_t)size;
  tx_lock(This);
  int32_t res = UART_write2(This->handle, &transaction);
  tx_unlock(This);
  HANDLE_TRANSFER_STATUS(transaction.status);
  LOG_FXN_TRACE(FINISH);
  return res == UART_SUCCESS ? size : 0;;
}

static void open(Drv_UART_t * This)
{
  UART_Params params;
  UART_Params_init(&params);
  params.baudRate = UART_BAUDRATE;
  params.writeDataMode = UART_DATA_BINARY;
  params.readDataMode = UART_DATA_BINARY;
  params.readReturnMode = UART_RETURN_FULL;
  This->handle = UART_open(UART_INSTANCE, &params);
  if (!This->handle)
  {
    LOG(WARNING, "open failure");
    return;
  }
}

bool init_Drv_UART(Drv_UART_t * This, Drv_UART_Cfg_t * cfg)
{
  This->rxBuf = cfg->rxBuf;
  This->rxBufSize = cfg->rxBufSize;

  This->Parent = 0;
  This->cbk.on_ready_read = 0;

  drv_UART_rxTask_isRunning = 0;

  Error_Block eb;
  Task_Params tskParams;

  Error_init(&eb);
  Task_Params_init(&tskParams);
  tskParams.instance->name = "sys.drv.uart.RX";
  tskParams.priority = SYS_TASK_DRV_UART_RX_PRIORITY;
  tskParams.arg0 = (UArg) This;
  This->rxTask = Task_create(drv_uart_rx_task_fxn, &tskParams, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create task failure");
    return false;
  }

  Error_init(&eb);
  This->txMutex = Semaphore_create(1, NULL, &eb);
  if (Error_check(&eb))
  {
    LOG(ERROR, "create semaphore failure");
    return false;
  }

  UART_init();
  open(This);

  return true;
}
