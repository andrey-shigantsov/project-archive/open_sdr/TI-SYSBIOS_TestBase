/*
 * uart.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_DRV_UART_H_
#define SYS_DRV_UART_H_

#include "../common.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct
{
  char * rxBuf;
  int rxBufSize;
} Drv_UART_Cfg_t;

typedef void (*drv_uart_rx_buf_ready_fxn_t)(void * This, uint8_t * rxBuf, int count);

typedef struct
{
  drv_uart_rx_buf_ready_fxn_t on_ready_read;
} Drv_UART_Cbk_t;

typedef struct
{
  void * handle;
  char * rxBuf;
  int rxBufSize;

  void * Parent;
  Drv_UART_Cbk_t cbk;

  void * rxTask;

  void * txMutex;
  void * rxMbx;
} Drv_UART_t;

bool init_Drv_UART(Drv_UART_t * This, Drv_UART_Cfg_t * cfg);

int32_t drv_uart_write(Drv_UART_t * This, const char * buf, size_t size);

void Drv_UART_read_handler(Drv_UART_t * This, uint8_t *buf, size_t count);

SYS_INLINE void Drv_UART_setCallback(Drv_UART_t * This, void * Parent, Drv_UART_Cbk_t * cbk)
{
  This->Parent = Parent;
  This->cbk = *cbk;
}

#ifdef __cplusplus
}
#endif

#endif /* SYS_DRV_UART_H_ */
