/*
 * spenttime.h
 *
 *  Created on: 19 окт. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_SPENTTIME_H_
#define SYS_SPENTTIME_H_

#ifdef SYS_SPENT_TIME_BIOS_CLOCK
#include <ti/sysbios/knl/Clock.h>

#define SYS_SPENT_TIME_INIT \
  volatile uint32_t begin, end;

#define SYS_SPENT_TIME_START \
    begin = Clock_getTicks();

#define SYS_SPENT_TIME_FINISH(float_res_us) \
    end = Clock_getTicks(); \
    float_res_us = (float)(end - begin) * Clock_tickPeriod;

#else
#include <SYS/common.h>
#include <SYS/timestamp.h>

#define SYS_SPENT_TIME_INIT \
  volatile long long _st_begin, _st_end, _st_overhead; \
  _st_begin = timestamp_clk(); \
  _st_end = timestamp_clk(); \
  _st_overhead = _st_end - _st_begin;

#define SYS_SPENT_TIME_START \
    _st_begin = timestamp_clk();

#define SYS_SPENT_TIME_FINISH(float_res_us) \
    _st_end = timestamp_clk(); \
    float_res_us = ((float)(_st_end - _st_begin - _st_overhead))*1000000/system_cpu_freq();

#endif
#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* SYS_SPENTTIME_H_ */
