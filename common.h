/*
 * common.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_COMMON_H_
#define SYS_COMMON_H_

#include <sys_cfg.h>

#include <stdint.h>
#include <stdbool.h>

#define SYS_INLINE static inline

#ifndef SYS_ASSERT
#include <assert.h>
#define SYS_ASSERT(x) assert(x)
#endif

#ifdef __cplusplus
extern "C"{
#endif

uint32_t system_cpu_freq();
uint32_t system_ticks_count_ms(int32_t timeout_ms);

#ifdef __cplusplus
}
#endif

#endif /* SYS_COMMON_H_ */
