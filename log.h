/*
 * log.h
 *
 *  Created on: 12 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef LOG_H_
#define LOG_H_

#include <xdc/runtime/System.h>

#ifndef LOG_NAME
#define LOG_NAME "unknown"
#endif

#ifndef LOG_LEVEL
#define LOG_LEVEL WARNING
#endif

typedef enum {loglvl_TRACE = -1, loglvl_DEBUG, loglvl_INFO, loglvl_WARNING, loglvl_ERROR, loglvl_FATAL} LOG_Lvl_t;
#define loglvl_xTRACE loglvl_TRACE

#define LOG_SPACES_FOR_LVL  "     "

#define LOG_LVL_TRACE       ">--->"
#define LOG_LVL_xTRACE      "  |  "
#define LOG_LVL_DEBUG       "(dbg)"
#define LOG_LVL_INFO        "     "
#define LOG_LVL_WARNING     "(wrn)"
#define LOG_LVL_ERROR       "!err!"
#define LOG_LVL_FATAL       "! FATAL ERROR !"

#define LOG_SUF_INIT "init successful"
#define LOG_SUF_START "started"
#define LOG_SUF_RUNNING "running..."
#define LOG_SUF_FINISH "finished"
#define LOG_SUF_OK "OK"
#define LOG_SUF_FAIL "FAIL"

#define LOG_RAW(format...) do{System_printf(format);System_printf("\n");}while(0)
#define LOG_RAW_MSG(name,format...) do{System_printf(name": "format);System_printf("\n");}while(0)
#define LOG_RAW_SUF(name,suf) do{System_printf(name": "LOG_SUF_##suf"\n");}while(0)
#define LOG_RAW_SUF_MSG(name,suf,format...) do{System_printf(name": "LOG_SUF_##suf": "format);System_printf("\n");}while(0)

#define LOG_LEVEL_xX(x) loglvl_##x
#define LOG_LEVEL_X(x) LOG_LEVEL_xX(x)

#define LOG_FORCE(lvl,format...) \
do{\
  System_printf(LOG_LVL_##lvl" "LOG_NAME": "format); \
  System_printf("\n");  \
}while(0)

#define LOG(lvl,format...) \
do{\
  if (LOG_LEVEL_X(lvl) >= LOG_LEVEL_X(LOG_LEVEL)) \
    LOG_FORCE(lvl,format); \
}while(0)

#define LOG_SUF(lvl,suf...) LOG(lvl,LOG_SUF_##suf)

#define LOG_FXN(lvl,format,...) LOG(lvl,"%s: "format,__func__,##__VA_ARGS__)
#define LOG_FXN_TRACE(suf) \
do{\
  if (LOG_LEVEL_X(TRACE) >= LOG_LEVEL_X(LOG_LEVEL)) \
  { \
    System_printf(LOG_LVL_TRACE" "LOG_NAME": %s: "LOG_SUF_##suf,__func__); \
    System_printf("\n");  \
  } \
}while(0)

#ifdef __cplusplus
extern "C"{
#endif



#ifdef __cplusplus
}
#endif

#endif /* LOG_H_ */
